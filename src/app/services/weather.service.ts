import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
   
@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  apiKey = 'a1b52e790aeb4157f2af4e04481ba688';
  URl: string = `https://api.openweathermap.org/data/2.5/weather?appid=${this.apiKey}&units=metric&q=`;

  constructor(private http: HttpClient) { }
  getWeather(cityName: string, countryCode: string){
      console.log("Request: "+this.URl+cityName+','+countryCode)
      return this.http.get(`${this.URl}${cityName},${countryCode}`);
  }
}
