import { Component, OnInit } from '@angular/core';
import { WeatherService } from '../services/weather.service';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { NgOptimizedImage } from '@angular/common';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
   weather: any;
   climaIcon : string = '';
   

   constructor(private weatherServices: WeatherService){

   }

  ngOnInit(): void {
    //throw new Error('Method not implemented.');
  }

  private handleError(error: HttpErrorResponse) {
    console.log(error)
    return false
    /*
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, body was: `, error.error);
    }
    // Return an observable with a user-facing error message.
    return throwError(() => new Error('Something bad happened; please try again later.'));
    */
  }


  getWeather(cityName: string, conuntryCode: string){
    this.weatherServices.getWeather(cityName, conuntryCode).subscribe(
      resp => {
        console.log(resp)
        this.weather = resp
        //this.climaIcon = resp.weather[0].icon;

      },
      err => {this.handleError,
        this.weather = false}//console.log(err)
    )

  }

  submitLocation(cityName: HTMLInputElement, countryCode: HTMLInputElement){
    
    console.log("Hola Mundo con servicios");
    console.log("Nombre de ciudad: "+cityName.value);
    console.log("Codigo de pais: "+countryCode.value);
    if ((cityName.value != '') && (countryCode.value != '')){
      this.getWeather(cityName.value, countryCode.value)
      cityName.value='';
      countryCode.value='';

    } else {
      this.simpleAlert ('Los campos no pueden estar vacios.');

    }
    return true;

  }

  simpleAlert(msj: string){
    Swal.fire(msj);
  }

  
}
